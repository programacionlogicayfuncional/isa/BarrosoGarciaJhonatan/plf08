(ns plf08.core-test
  (:require [clojure.test :refer :all]
            [plf08.core :refer :all]))

(deftest a-test
  (testing "codificar letra a"
    (is (= "♜♜" (codificar "a"))))
  (testing "codificar palabra Una"
    (is (= "♝♖♞♘♜♜" (codificar "Una"))))
  (testing "codificar palabra mañana"
    (is (= "♞♖♜♜♞♗♜♜♞♘♜♜♚♔" (codificar "mañana,"))))
  (testing "codificar palabra Numerosas"
    (is (= "♞♘♝♖♞♖♜♖♝♝♞♕♝♛♜♜♝♛" (codificar "Numerosas"))))
  (testing "codificar palabra SOBRE"
    (is (= "♝♛♞♕♜♝♝♝♜♖" (codificar "SOBRE"))))
  (testing " sobre"
    (is (= " ♝♛♞♕♜♝♝♝♜♖" (codificar " sobre"))))
  (testing "codificar palabra la"
    (is (= "♞♚♜♜" (codificar "la"))))
  (testing "codificar palabra mesa"
    (is (= "♞♖♜♖♝♛♜♜" (codificar "mesa"))))
  ;decodificar
  (testing "decodificar letra ♜♜"
    (is (= "a" (decodificar "♜♜"))))
  (testing "decodificar palabra ♝♖♞♘♜♜"
    (is (= "una" (decodificar "♝♖♞♘♜♜"))))
  (testing "decodificar palabra♞♖♜♜♞♗♜♜♞♘♜♜♚♔"
    (is (= "mañana," (decodificar "♞♖♜♜♞♗♜♜♞♘♜♜♚♔"))))
  (testing "decodificar palabra ♞♘♝♖♞♖♜♖♝♝♞♕♝♛♜♜♝♛"
    (is (= "numerosas" (decodificar "♞♘♝♖♞♖♜♖♝♝♞♕♝♛♜♜♝♛"))))
  (testing "decodificar palabra SOBRE"
    (is (= "sobre" (decodificar "♝♛♞♕♜♝♝♝♜♖"))))
  (testing "decodificar  ♝♛♞♕♜♝♝♝♜♖"
    (is (= " sobre" (decodificar " ♝♛♞♕♜♝♝♝♜♖"))))
  (testing "decodificar palabra la"
    (is (= "la" (decodificar "♞♚♜♜"))))
  (testing "decodificar palabra mesa"
    (is (= "mesa" (decodificar "♞♖♜♖♝♛♜♜")))))