(ns plf08.core
  (:gen-class))

(defn tabla-conversion
  []
  (hash-map \a	[\♜	\♜]
            \á	[\♜	\♞]
            \b	[\♜	\♝]
            \c	[\♜	\♛]
            \d	[\♜	\♚]
            \e	[\♜	\♖]
            \é	[\♜	\♘]
            \f	[\♜	\♗]
            \g	[\♜	\♕]
            \h	[\♜	\♔]
            \i	[\♞	\♜]
            \í	[\♞	\♞]
            \j	[\♞	\♝]
            \k	[\♞	\♛]
            \l	[\♞	\♚]
            \m	[\♞	\♖]
            \n	[\♞	\♘]
            \ñ	[\♞	\♗]
            \o	[\♞	\♕]
            \ó	[\♞	\♔]
            \p	[\♝	\♜]
            \q	[\♝	\♞]
            \r	[\♝	\♝]
            \s	[\♝	\♛]
            \t	[\♝	\♚]
            \u	[\♝	\♖]
            \ú	[\♝	\♘]
            \ü	[\♝	\♗]
            \v	[\♝	\♕]
            \w	[\♝	\♔]
            \x	[\♛	\♜]
            \y	[\♛	\♞]
            \z	[\♛	\♝]
            \0	[\♛	\♛]
            \1	[\♛	\♚]
            \2	[\♛	\♖]
            \3	[\♛	\♘]
            \4	[\♛	\♗]
            \!	[\♛	\♕]
            \"	[\♛	\♔]
            \#	[\♚	\♜]
            \$	[\♚	\♞]
            \%	[\♚	\♝]
            \&	[\♚	\♛]
            \'	[\♚	\♚]
            \(	[\♚	\♖]
            \)	[\♚	\♘]
            \*	[\♚	\♗]
            \+	[\♚	\♕]
            \,	[\♚	\♔]
            \-	[\♖	\♜]
            \.	[\♖	\♞]
            \/	[\♖	\♝]
            \:	[\♖	\♛]
            \;	[\♖	\♚]
            \<	[\♖	\♖]
            \=	[\♖	\♘]
            \>	[\♖	\♗]
            \?	[\♖	\♕]
            \@	[\♖	\♔]
            \[	[\♘	\♜]
            \\	[\♘	\♞]
            \]	[\♘	\♝]
            \^	[\♘	\♛]
            \_	[\♘	\♚]
            \`	[\♘	\♖]
            \{	[\♘	\♘]
            \|	[\♘	\♗]
            \}	[\♘	\♕]
            \~	[\♘	\♔]
            \5	[\♗	\♜]
            \6	[\♗	\♞]
            \7	[\♗	\♝]
            \8	[\♗	\♛]
            \9	[\♗	\♚]
            \A  [\♜	\♜]
            \Á	[\♜	\♞]
            \B	[\♜	\♝]
            \C	[\♜	\♛]
            \D	[\♜	\♚]
            \E	[\♜	\♖]
            \É	[\♜	\♘]
            \F	[\♜	\♗]
            \G	[\♜	\♕]
            \H  [\♜	\♔]
            \I	[\♞	\♜]
            \Í	[\♞	\♞]
            \J	[\♞	\♝]
            \K	[\♞	\♛]
            \L	[\♞	\♚]
            \M	[\♞	\♖]
            \N	[\♞	\♘]
            \Ñ	[\♞	\♗]
            \O	[\♞	\♕]
            \Ó	[\♞	\♔]
            \P	[\♝	\♜]
            \Q	[\♝	\♞]
            \R  [\♝	\♝]
            \S	[\♝	\♛]
            \T	[\♝	\♚]
            \U	[\♝	\♖]
            \Ú	[\♝	\♘]
            \Ü	[\♝	\♗]
            \V	[\♝	\♕]
            \W	[\♝	\♔]
            \X	[\♛	\♜]
            \Y	[\♛	\♞]
            \Z	[\♛	\♝]))
;Codificar la cadena 

(defn codificar
  [xs]
  (let [tabla (tabla-conversion)
        convertir (fn [c] (let [v (apply str (tabla c))] (if (empty? v) c v)))]
    (apply str (map convertir xs))))

(codificar " ola")
(codificar "Hola")
(codificar "HOLA")

;Decodificar la cadema 

(defn decodificar
  [s]
  (let [tabla (zipmap (vals (tabla-conversion)) (keys (tabla-conversion)))
        separar (fn [] (loop [xs s
                              result []]
                         (if  xs
                           (if (or (= (first xs) \♜) (= (first xs) \♞) (= (first xs)  \♝) (= (first xs) \♛) (= (first xs) \♚) (= (first xs) \♖) (= (first xs)  \♘) (= (first xs) \♗) (= (first xs) \♕) (= (first xs)  \♔))
                             (recur (next (next xs)) (conj result [(first xs) (second xs)]))
                             (recur (next xs) (conj result [(first xs)])))
                           result)))
        convertir (fn [xs] (let [v (tabla xs)] (if (nil? v) (apply str xs) v)))]
    (apply str (map convertir (separar)))))

(decodificar "pedro ♜♔♞♕♞♚♜♜ hola")

(codificar  (slurp "./resources/kafka.txt"))

(decodificar (codificar  (slurp "./resources/kafka.txt")))
(spit "./resources/res.txt" (decodificar (codificar  (slurp "./resources/kafka.txt"))))



;Primer argumento es opción (codificar | decodificar)
;Segundo argumento es el nombre del archivo a codificar o decodificar
;Tercer argumento es el nombre del archivo que tendra el resultado

(defn -main
  [& args]
  (if (empty? args)
    (println "Error, faltan argumentos.")
    (if (= "codificar" (apply str (first args)))
      (spit (apply str (rest (rest args))) (codificar (slurp (apply str (second args)))))
      (spit (apply str (rest (rest args))) (decodificar (slurp (apply str (second args))))))))