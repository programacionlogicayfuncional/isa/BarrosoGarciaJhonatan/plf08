# plf08

# Descripción del proyecto.

Aplicación que lleva a cabo el cifrado y descifrado mediante el Cuadrado de Polibio.

# Forma de ejecutar.

Terminal del SO que se este usando
 - lein run "codificar" "./resources/kafka.txt" "./resources/res.txt"

# Opciones para su ejecución.

Para la ejecución es necesario introducir un texto, a partir de un texto original no cifrado se puede obtener un texto cifrado y a partir de este texto cifrado se puede obtener el texto original no cifrado.

# Ejemplos de uso o ejecución.

lein run "codificar" "./resources/kafka.txt" "./resources/res.txt"

lein run "decodificar" "./resources/res.txt" "./resources/res.txt"


# Errores, limitantes, situaciones no consideradas o particularidades de tu solución.

Siempre será necesario que se ejecute con tres parametros 
1. Primer argumento es opción (codificar | decodificar)
2. Segundo argumento es el nombre del archivo a codificar o decodificar
3. Tercer argumento es el nombre del archivo que tendra el resultado

FIXME: description

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar plf08-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful


## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
